package com.example.lisad.rl_corp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{
    ImageView go;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        go=(ImageView) this.findViewById(R.id.imageView);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Bienvenue sur la page de connexion !", Toast.LENGTH_LONG).show();
                Intent intentJeu = new Intent(MainActivity.this,LoginActivity.class);
                startActivityForResult(intentJeu,10);
            }
        });

    }

}
