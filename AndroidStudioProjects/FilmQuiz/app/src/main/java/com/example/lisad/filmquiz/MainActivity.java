package com.example.lisad.filmquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText prenom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button go = (Button) findViewById(R.id.btn_go);
        go.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intentJeu = new Intent(MainActivity.this, JeuActivity.class);
                startActivityForResult(intentJeu,10);

                String leprenom = prenom.getText().toString();
                intentJeu.putExtra("Joueur", leprenom);
                intentJeu.putExtra("Numero", 1);
                // lancement de l’activité
                Toast.makeText(MainActivity.this, "Champs vide !", Toast.LENGTH_LONG).show();

            }
            });
    }
}